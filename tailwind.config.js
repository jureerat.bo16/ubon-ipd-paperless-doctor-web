/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      screens: {
        'xs': '299px',
        // => @media (min-width: 640px) { ... }384
        'sm': '600px',
        // => @media (min-width: 640px) { ... }775

        'md': '950px',
        // => @media (min-width: 768px) { ... }

        'lg': '1200px',
        // => @media (min-width: 1024px) { ... }

        'xl': '1900px',
        // => @media (min-width: 1280px) { ... }
        'xxl': '2136px'
      },
    },
  },
  plugins: [],
}

