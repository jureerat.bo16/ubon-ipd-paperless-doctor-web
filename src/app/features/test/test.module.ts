import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { TestComponent } from './test.component';

import { NgZorroModule } from '../../ng-zorro.module';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { SharedModule } from '../../shared/shared.module';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';

// import { HeaderComponent } from './header/header.component';



import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TestComponent
  ],
  imports: [
    CommonModule,
    TestRoutingModule,
   
    NgZorroModule,
    // SharedModule,
    NzSwitchModule,
    NzLayoutModule,
    NzMenuModule,FormsModule
  ]
})
export class TestModule { }
