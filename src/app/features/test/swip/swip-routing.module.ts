import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SwipComponent } from './swip.component';

const routes: Routes = [{ path: '', component: SwipComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SwipRoutingModule { }
