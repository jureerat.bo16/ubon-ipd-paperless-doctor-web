import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EkgRoutingModule } from './ekg-routing.module';
import { EkgComponent } from './ekg.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    EkgComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    EkgRoutingModule
  ]
})
export class EkgModule { }
